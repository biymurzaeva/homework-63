import './App.css';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import Blog from "./containers/Blog/Blog";
import FormPost from "./components/FormPost/FormPost";
import Header from "./components/Header/Header";
import About from "./containers/About/About";
import Contacts from "./containers/Contacts/Contacts";
import FullPost from "./containers/FullPost/FullPost";
import EditPost from "./containers/EditPost/EditPost";

const App = () => (
  <BrowserRouter>
	  <Header/>
	  <Switch>
	    <Route path="/" exact component={Blog}/>
	    <Route path="/posts/add" component={FormPost}/>
		  <Route path="/about" component={About}/>
		  <Route path="/contacts" component={Contacts}/>
		  <Route path="/posts/:id" component={FullPost}/>
		  <Route path="/:id/edit" component={EditPost}/>

      <Route render={() => <h1>Not Fount</h1>}/>
    </Switch>
  </BrowserRouter>
);

export default App;
