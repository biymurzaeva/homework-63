import React, {useState} from 'react';
import dayjs from "dayjs";
import './FormPost.css';
import axiosApi from "../../axiosApi";

const FormPost = () => {
	const [post, setPost] = useState({
		title: '',
		description: '',
		day: dayjs().format('DD.MM.YYYY HH:mm:ss')
	});

	const onInputChange = e => {
		const {name, value} = e.target;

		setPost(prev => ({
			...prev,
			[name]: value
		}));
	};

	const createPost = async e => {
		e.preventDefault();

		try {
			await axiosApi.post('/posts.json', {
				post
			});
		} finally {
			setPost(prev => ({
				...prev,
				title: '',
				description: '',
				day: dayjs().format('DD.MM.YYYY HH:mm:ss')
			}));
		}
	};

	return (
		<div className="container">
			<div className="add-post-page">
				<h2>Add new post</h2>
				<div className="form-block">
					<form onSubmit={createPost}>
						<div className="form-row">
							<label>Title:</label>
							<input
								type="text"
								name="title"
								value={post.title}
								onChange={onInputChange}
							/>
						</div>
						<div className="form-row">
							<label>Description:</label>
							<textarea
								rows="10"
								name="description"
								value={post.description}
								onChange={onInputChange}
							/>
						</div>
						<button type="submit" className="btn-save">Save</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default FormPost;