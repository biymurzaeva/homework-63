import React from 'react';
import {Link} from "react-router-dom";
import './Header.css';

const Header = () => {
	return (
		<div className="container">
			<header className="header">
				<h3>My Blog</h3>
				<nav className="main-nav">
					<ul>
						<li><Link to="/">Home</Link></li>
						<li><Link to="/posts/add">Add</Link></li>
						<li><Link to="/about">About</Link></li>
						<li><Link to="/contacts">Contacts</Link></li>
					</ul>
				</nav>
			</header>
		</div>
	);
};

export default Header;