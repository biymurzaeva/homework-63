import React from 'react';
import './Post.css';

const Post = props => {
	return (
		<div className="post">
			<p>Created on: {props.day}</p>
			<h3>{props.title}</h3>
			<button onClick={e => props.clickedReadMore(e.currentTarget, props.id)}>Read more >></button>
		</div>
	);
};

export default Post;