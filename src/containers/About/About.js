import React from 'react';

const About = () => {
	return (
		<div className="container">
			<h2>About Us</h2>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium ad amet, animi aspernatur deleniti eius
				error esse, expedita, incidunt labore odio optio perferendis porro quidem quis quod repellendus. Animi,
				pariatur.</p>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Alias debitis dolor eius illo laudantium magni quo
				repudiandae sint, sunt velit! Aliquam deserunt dignissimos excepturi id odio perferendis possimus praesentium
				quod.</p>
		</div>
	);
};

export default About;