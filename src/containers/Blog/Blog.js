import React, {useEffect, useState} from 'react';
import './Blog.css';
import Post from "../../components/Post/Post";
import axiosApi from "../../axiosApi";

const Blog = ({history}) => {
	const [posts, setPosts] = useState(null);
	const [postId, setPostId] = useState(null);

	useEffect(() => {
		const fetchData  = async () => {
			const response =  await axiosApi.get('/posts.json');

			if (response.data) {
				setPostId(Object.keys(response.data));

				const promises = Object.keys(response.data).map(ID => {
					return axiosApi.get(`/posts/${ID}.json`);
				});

				const results = await Promise.all(promises);
				setPosts(results);
			}
		};

		fetchData().catch(console.error);
	}, []);

	const onClickBtn = (e, i) => {
		history.push(`/posts/${i}`);
	}

	return posts && (
		<div className="container">
			{posts.map((post, i) => (
				<Post
					key={post.data.post.day}
					day={post.data.post.day}
					title={post.data.post.title}
					id={postId[i]}
					clickedReadMore={onClickBtn}
				/>
			))}
		</div>
	);
};

export default Blog;