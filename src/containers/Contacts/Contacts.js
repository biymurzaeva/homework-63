import React, {useState} from 'react';
import axiosApi from "../../axiosApi";
import './Contacts.css';

const Contacts = () => {
	const [message, setMessage] = useState({
		name: '',
		email: '',
		message: ''
	});

	const onInputChange = e => {
		const {name, value} = e.target;

		setMessage(prev => ({
			...prev,
			[name]: value
		}));
	};

	const sendMessage = async e => {
		e.preventDefault();

		try {
			await axiosApi.post('/messages.json', {
				message
			});
		} finally {
			setMessage(prev => ({
				...prev,
				name: '',
				email: '',
				message: ''
			}));
		}
	};

	return (
		<div className="container">
			<div className="contacts-block">
				<div className="contacts">
					<h4>Call Us</h4>
					<p>+111(255) 45-88-32</p>
					<p>+111(277) 45-88-32</p>
					<h4>Locution</h4>
					<p>125 Some Street, NY 24587 </p>
					<h4>Email</h4>
					<p>someone@dsw.fd</p>
				</div>
				<div className="contacts-form">
					<form onSubmit={sendMessage}>
						<div className="form-row">
							<input
								type="text"
								name="name"
								placeholder="Enter your name"
								value={message.name}
								onChange={onInputChange}
							/>
						</div>
						<div className="form-row">
							<input
								type="email"
								name="email"
								placeholder="Enter a valid your email address"
								value={message.email}
								onChange={onInputChange}
							/>
						</div>
						<div className="form-row">
							<textarea
								rows="6"
								name="message"
								placeholder="Enter your message"
								value={message.message}
								onChange={onInputChange}
							/>
						</div>
						<button type="submit">Submit</button>
					</form>
				</div>
			</div>
		</div>
	);
};

export default Contacts;