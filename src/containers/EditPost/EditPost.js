import React, {useState} from 'react';
import axiosApi from "../../axiosApi";

const EditPost = ({location, history, match}) => {
		const parseParam = () => {
			const params = new URLSearchParams(location.search);
			return Object.fromEntries(params);
		};

		const [post, setPost] = useState(parseParam());

		const onInputChange = e => {
			const {name, value} = e.target;

			setPost(prev => ({
				...prev,
				[name]: value
			}));
		};

		const updatePost = async e => {
			e.preventDefault();

			try {
				await axiosApi.put(`/posts/${match.params.id}.json`, {
					post: post
				});
			} finally {
				history.replace('/');
			}
		};

		return (
			<div className="container">
				<div className="add-post-page">
					<h2>Edit post</h2>
					<div className="form-block">
						<form onSubmit={updatePost}>
							<div className="form-row">
								<label>Title:</label>
								<input
									type="text"
									name="title"
									value={post.title}
									onChange={onInputChange}
								/>
							</div>
							<div className="form-row">
								<label>Description:</label>
								<textarea
									rows="10"
									name="description"
									value={post.description}
									onChange={onInputChange}
								/>
							</div>
							<button type="submit" className="btn-save">Update</button>
						</form>
					</div>
				</div>
			</div>
		);
};

export default EditPost;