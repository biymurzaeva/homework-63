import React, {useEffect, useState} from 'react';
import axiosApi from "../../axiosApi";
import './FullPost.css';

const FullPost = ({match, history}) => {
	const [post, setPost] = useState(null);

	useEffect(() => {
		const fetchData = async () => {
			const response = await axiosApi.get(`/posts/${match.params.id}.json`);
			setPost(response.data);
		};

		fetchData().catch(console.error);
	}, [match.params.id]);

	const deletePost = async (e, postId) => {
		try {
			await axiosApi.delete(`/posts/${postId}.json`);
		} finally {
			history.replace("/");
		}
	};

	const editPost = () => {
		const params = new URLSearchParams(post.post);
		history.push(`/${match.params.id}/edit?` + params.toString());
	};

	return post && (
		<div className="container">
			<div className="fullPost">
				<p className="postDay">Created on: {post.post.day}</p>
				<h3>{post.post.title}</h3>
				<p>{post.post.description}</p>
				<hr/>
				<div className="btns-block">
					<button onClick={e => deletePost(e.currentTarget, match.params.id)}>Delete</button>
					<button onClick={() => editPost()}>Edit</button>
				</div>
			</div>
		</div>
	);
};

export default FullPost;